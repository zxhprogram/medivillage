package szczepanski.gerard.medivillage.program.medivillage;

import javafx.application.Application;
import javafx.scene.canvas.Canvas;
import javafx.stage.Stage;
import szczepanski.gerard.medivillage.engine.engine.Engine;
import szczepanski.gerard.medivillage.engine.engine.EngineFactory;
import szczepanski.gerard.medivillage.engine.scenario.GameScenario;
import szczepanski.gerard.medivillage.view.window.GameScene;

public class Medivillage extends Application {
	
	public static void main(String... args) {
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		GameScene gameScene = GameScene.create();
		Engine engine = createEngine(gameScene);
		engine.start();
		
		primaryStage.setTitle("Medivillage");
		primaryStage.setResizable(false);
		primaryStage.setScene(gameScene.getScene());
		primaryStage.show();
	}
	
	private Engine createEngine(GameScene gameScene) {
		Canvas canvas = gameScene.getCanvas();
		EngineFactory engineFactory = new EngineFactory();
		return engineFactory.newGameEngine(canvas, new GameScenario("testGame"));
	}
	
}
