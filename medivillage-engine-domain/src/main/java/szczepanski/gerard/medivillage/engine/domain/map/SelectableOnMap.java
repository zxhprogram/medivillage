package szczepanski.gerard.medivillage.engine.domain.map;

import javafx.scene.canvas.GraphicsContext;
import szczepanski.gerard.medivillage.engine.domain.sprite.Bounds;

public interface SelectableOnMap {
	
	String getId();
	
	Bounds bounds();
	
	void drawSelected(GraphicsContext context);
	
}
