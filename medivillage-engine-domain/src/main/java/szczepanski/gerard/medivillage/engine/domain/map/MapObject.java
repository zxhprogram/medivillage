package szczepanski.gerard.medivillage.engine.domain.map;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.medivillage.engine.domain.map.terrain.TerrainTile;
import szczepanski.gerard.medivillage.engine.domain.map.utils.Isometric;

/**
 * An object that can be presented on Map.
 * @author Gerard Szczepanski
 */
@RequiredArgsConstructor
public abstract class MapObject implements Drawable {
	
	private final TerrainTile tile;
	protected final Image image;

	@Override
	public void draw(GraphicsContext context) {
		context.save();
		context.translate(tile.getX(), tile.getY());
		
		context.drawImage(image, tile.getX(), tile.getY(), Isometric.TILE_WIDTH, Isometric.TILE_HEIGHT);
		context.restore();
	}
	
}
