package szczepanski.gerard.medivillage.engine.domain.character.villager;

import java.util.concurrent.atomic.AtomicLong;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class VillagerIdGenerator {
	
	private static final String PREFIX = "VILLAGER_";
	private static final AtomicLong ID_GENERTOR = new AtomicLong(-1);
	
	public static String generateId() {
		return PREFIX + ID_GENERTOR.incrementAndGet();
	}
	
}
