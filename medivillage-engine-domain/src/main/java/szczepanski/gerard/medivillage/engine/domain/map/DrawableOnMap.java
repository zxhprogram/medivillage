package szczepanski.gerard.medivillage.engine.domain.map;

public interface DrawableOnMap extends Drawable {
	
	Point mapPoint();
	
}
