package szczepanski.gerard.medivillage.engine.domain.common;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;

import szczepanski.gerard.medivillage.common.collections.CollectionFactory;

public abstract class EntityRegistry<V extends Entity> {

	private final Map<String, V> registry = CollectionFactory.map();

	public void add(V value) {
		registry.put(value.getId(), value);
	}
	
	public V find(String key) {
		return registry.get(key);
	}
	
	public Optional<V> findSafe(String key) {
		return Optional.ofNullable(registry.get(key));
	}
	
	public Collection<V> findAll() {
		return registry.values();
	}

}
