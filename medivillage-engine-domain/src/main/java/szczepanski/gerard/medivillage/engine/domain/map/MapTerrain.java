package szczepanski.gerard.medivillage.engine.domain.map;

import javafx.scene.canvas.GraphicsContext;
import szczepanski.gerard.medivillage.common.util.ParamAssertion;
import szczepanski.gerard.medivillage.engine.domain.map.terrain.TerrainMove;
import szczepanski.gerard.medivillage.engine.domain.map.terrain.TerrainTile;
import szczepanski.gerard.medivillage.engine.domain.map.terrain.TerrainTileFactory;
import szczepanski.gerard.medivillage.engine.domain.map.terrain.TerrainType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class MapTerrain implements Drawable {

    private final int numberOfTiles;
    private final Map<Point, TerrainTile> terrainTilesMap;
    private final List<TerrainTile> terrainTiles;

    public MapTerrain(int numberOfTiles) {
        this.numberOfTiles = numberOfTiles;
        int terrainCapacity = numberOfTiles * numberOfTiles;
        this.terrainTilesMap = new HashMap<>(terrainCapacity);
        this.terrainTiles = new ArrayList<>(terrainCapacity);
        populateTerrainTiles(numberOfTiles);
    }

    private void populateTerrainTiles(int numberOfTiles) {
        TerrainTileFactory tileFactory = new TerrainTileFactory();

        for (double x = 0; x < numberOfTiles; x++) {
            for (double y = 0; y < numberOfTiles; y++) {
                TerrainType terrainType = TerrainType.GRASS;
                Point tilePoint = new Point(x, y);

                TerrainTile tile = tileFactory.create(tilePoint, terrainType);
                terrainTilesMap.put(tilePoint, tile);
                terrainTiles.add(tile);
            }
        }
    }

    @Override
    public void draw(GraphicsContext context) {
        this.terrainTiles.forEach(t -> t.draw(context));
    }

    public TerrainTile tileOfIso(Point point) {
        ParamAssertion.isNotNull(point);

        for (TerrainTile tile : terrainTiles) {
            if (tile.containsPoint(point)) {
                return tile;
            }
        }
        return null;
    }


    public TerrainTile tileOfCart(Point point) {
        ParamAssertion.isNotNull(point);
        return terrainTilesMap.get(point.toPointWithoutDecimalValues());
    }

    public TerrainTile tileOfId(int id) {
        return terrainTiles.get(id);
    }

    public boolean isMoveOnTilePossible(Point spritePoint, TerrainMove nextMove) {
        double movedSpriteX = spritePoint.getX() + nextMove.getCurrentTileXOffset();
        double movedSpriteY = spritePoint.getY() + nextMove.getCurrentTileYOffset();

        return movedSpriteX >= 0.0 && movedSpriteX <= numberOfTiles - 1 && movedSpriteY >= 0.0 && movedSpriteY <= numberOfTiles - 1;
    }

    //TMP naive impl
    public TerrainMove nextMoveForTile(TerrainTile sourceTile, TerrainTile destinationTile) {
        if (sourceTile.getX() > destinationTile.getX()) {
            if (sourceTile.getY() > destinationTile.getY()) {
                return TerrainMove.NORTH_WEST;
            } else if (sourceTile.getY() < destinationTile.getY()) {
                return TerrainMove.SOUTH_WEST;
            } else {
                return TerrainMove.WEST;
            }
        } else if (sourceTile.getX() < destinationTile.getX()) {
            if (sourceTile.getY() > destinationTile.getY()) {
                return TerrainMove.NORTH_EAST;
            } else if (sourceTile.getY() < destinationTile.getY()) {
                return TerrainMove.SOUTH_EAST;
            } else {
                return TerrainMove.EAST;
            }
        } else {
            if (sourceTile.getY() > destinationTile.getY()) {
                return TerrainMove.NORTH;
            } else if (sourceTile.getY() < destinationTile.getY()) {
                return TerrainMove.SOUTH;
            } else {
                return TerrainMove.NO_MOVE;
            }
        }
    }
}
