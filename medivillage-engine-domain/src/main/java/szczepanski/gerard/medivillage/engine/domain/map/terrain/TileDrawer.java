package szczepanski.gerard.medivillage.engine.domain.map.terrain;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import szczepanski.gerard.medivillage.engine.domain.map.utils.Isometric;

final class TileDrawer {

    public static void draw(TileCoordinates coordinates, TerrainType terrain, GraphicsContext context) {
        context.save();
        context.translate(coordinates.getIsoPoint().getX(), coordinates.getIsoPoint().getY());

        Image image = terrain.getImage();
        context.drawImage(image, coordinates.getCartesianPoint().getX(), coordinates.getCartesianPoint().getY(), Isometric.TILE_WIDTH, Isometric.TILE_HEIGHT);

        context.restore();
    }

}
