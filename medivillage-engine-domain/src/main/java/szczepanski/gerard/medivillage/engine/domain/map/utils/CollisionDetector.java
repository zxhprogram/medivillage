package szczepanski.gerard.medivillage.engine.domain.map.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import szczepanski.gerard.medivillage.common.util.ParamAssertion;
import szczepanski.gerard.medivillage.engine.domain.map.Point;
import szczepanski.gerard.medivillage.engine.domain.sprite.Bounds;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class CollisionDetector {
	
	public static boolean isPointInBounds(Point point, Bounds bounds) {
		ParamAssertion.isNotNull(point, bounds);
		
		return point.getX() >= bounds.getX() && point.getX() <= bounds.getX() + bounds.getWidth() && 
				point.getY() >= bounds.getY() && point.getY() <= bounds.getY() + bounds.getHeight();
	}
	
}
