package szczepanski.gerard.medivillage.engine.domain.map;

import javafx.scene.canvas.GraphicsContext;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.medivillage.engine.domain.map.terrain.TerrainMove;
import szczepanski.gerard.medivillage.engine.domain.map.terrain.TerrainTile;

@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public class GameMap implements Drawable {

    private final MapTerrain terrain;

    public TerrainTile tileOfIso(Point point) {
       return terrain.tileOfIso(point);
    }

    public TerrainTile tileOfCart(Point point) {
        return terrain.tileOfCart(point);
    }

    public boolean isMoveOnTilePossible(Point spritePoint, TerrainMove nextMove) {
        return terrain.isMoveOnTilePossible(spritePoint, nextMove);
    }

    public TerrainMove nextMoveForTile(TerrainTile sourceTile, TerrainTile destinationTile) {
        return terrain.nextMoveForTile(sourceTile, destinationTile);
    }

    @Override
    public void draw(GraphicsContext context) {
        terrain.draw(context);
    }
}
