package szczepanski.gerard.medivillage.engine.domain.character.villager;

import javafx.scene.image.Image;
import szczepanski.gerard.medivillage.engine.domain.character.GameCharacter;
import szczepanski.gerard.medivillage.engine.domain.map.MapObjectHeight;
import szczepanski.gerard.medivillage.engine.domain.map.terrain.TerrainTile;
import szczepanski.gerard.medivillage.engine.domain.map.terrain.TerrainType;
import szczepanski.gerard.medivillage.engine.domain.map.utils.Isometric;

public class Villager extends GameCharacter {
	
	private static final Image IMG = new Image(TerrainType.class.getResourceAsStream("/images/sprites/villager.png"), Isometric.TILE_WIDTH, Isometric.TILE_WIDTH, true, true);

	public Villager(TerrainTile currentTile) {
		super(VillagerIdGenerator.generateId(), IMG, MapObjectHeight.MEDIUM, currentTile);
	}
	
}
