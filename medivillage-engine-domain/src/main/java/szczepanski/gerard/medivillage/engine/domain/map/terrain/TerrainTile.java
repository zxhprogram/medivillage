package szczepanski.gerard.medivillage.engine.domain.map.terrain;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import szczepanski.gerard.medivillage.engine.domain.map.Drawable;
import szczepanski.gerard.medivillage.engine.domain.map.Point;
import szczepanski.gerard.medivillage.engine.domain.map.utils.Isometric;

@Getter
@Builder
@AllArgsConstructor
public final class TerrainTile implements Drawable {

	private final int id;
	private final TileCoordinates coordinates;
	private TerrainType terrainType;

	@Override
	public void draw(GraphicsContext context) {
		TileDrawer.draw(coordinates, terrainType, context);
	}

	public double getX() {
		return coordinates.getCartesianPoint().getX();
	}

	public double getY() {
		return coordinates.getCartesianPoint().getY();
	}

	public boolean containsPoint(Point point) {
		return coordinates.getPolygon().contains(point.getX(), point.getY());
	}

}
