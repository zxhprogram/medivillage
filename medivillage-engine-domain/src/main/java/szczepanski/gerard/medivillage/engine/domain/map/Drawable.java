package szczepanski.gerard.medivillage.engine.domain.map;

import javafx.scene.canvas.GraphicsContext;

public interface Drawable {
	
	void draw(GraphicsContext context);
	
}
