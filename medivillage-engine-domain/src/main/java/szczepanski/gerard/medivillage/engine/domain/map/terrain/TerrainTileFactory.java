package szczepanski.gerard.medivillage.engine.domain.map.terrain;

import com.google.common.base.Preconditions;
import szczepanski.gerard.medivillage.engine.domain.map.Point;

import java.util.concurrent.atomic.AtomicInteger;

public class TerrainTileFactory {

    private final AtomicInteger idGenerator = new AtomicInteger(0);

    public TerrainTile create(Point tilePoint, TerrainType terrainType) {
        Preconditions.checkNotNull(tilePoint);
        Preconditions.checkNotNull(terrainType);

        return TerrainTile.builder()
                .id(idGenerator.incrementAndGet())
                .terrainType(terrainType)
                .coordinates(TileCoordinates.fromPoint(tilePoint))
                .build();
    }


}
