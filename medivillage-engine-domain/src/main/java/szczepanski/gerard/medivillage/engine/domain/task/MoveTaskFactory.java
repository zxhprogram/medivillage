package szczepanski.gerard.medivillage.engine.domain.task;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.medivillage.engine.domain.character.GameCharacter;
import szczepanski.gerard.medivillage.engine.domain.map.GameMap;
import szczepanski.gerard.medivillage.engine.domain.map.terrain.TerrainTile;

@RequiredArgsConstructor
public class MoveTaskFactory {
	
	private final GameMap map;
	
	public MoveTask create(GameCharacter character, TerrainTile targetMovementTile) {
		return new MoveTask(map, character, targetMovementTile);	
	}

}
