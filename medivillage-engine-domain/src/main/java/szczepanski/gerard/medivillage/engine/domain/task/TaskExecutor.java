package szczepanski.gerard.medivillage.engine.domain.task;

import java.util.Optional;

public interface TaskExecutor {
	
	Optional<Task> currentTask();
	
	void removeTask();
	
	void updateTask();
	
}
