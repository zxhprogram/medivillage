package szczepanski.gerard.medivillage.engine.domain.character;

import java.util.Optional;

import javafx.scene.image.Image;
import szczepanski.gerard.medivillage.engine.domain.map.MapObjectHeight;
import szczepanski.gerard.medivillage.engine.domain.map.terrain.TerrainTile;
import szczepanski.gerard.medivillage.engine.domain.sprite.Sprite;
import szczepanski.gerard.medivillage.engine.domain.task.MoveTask;
import szczepanski.gerard.medivillage.engine.domain.task.Task;
import szczepanski.gerard.medivillage.engine.domain.task.TaskExecutor;

public abstract class GameCharacter extends Sprite implements TaskExecutor {

	private Task task;

	public GameCharacter(String id, Image image, MapObjectHeight height, TerrainTile currentTile) {
		super(id, image, height, currentTile);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Optional<Task> currentTask() {
		return Optional.ofNullable(this.task);
	}

	@Override
	public void removeTask() {
		this.task = null;
	}

	@Override
	public void updateTask() {
		if (task != null) {
			this.task.update();
		}
	}
	
	public void applyMoveTask(MoveTask task) {
		this.task = task;
	}
}
