package szczepanski.gerard.medivillage.engine.domain.map.terrain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum TerrainMove {
	
	NO_MOVE(0, 0),
	NORTH(0, -0.1),
	NORTH_EAST(+0.1, -0.1),
	EAST(0.1, 0),
	SOUTH_EAST(0.1, 0.1),
	SOUTH(0, 0.1),
	SOUTH_WEST(-0.1, 0.1),
	WEST(-0.1, 0),
	NORTH_WEST(-0.1, -0.1);
	
	private final double currentTileXOffset;
	private final double currentTileYOffset;

}
