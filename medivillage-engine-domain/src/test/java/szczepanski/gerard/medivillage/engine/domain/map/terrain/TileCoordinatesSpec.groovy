package szczepanski.gerard.medivillage.engine.domain.map.terrain

import spock.lang.Specification
import szczepanski.gerard.medivillage.engine.domain.map.Point

class TileCoordinatesSpec extends Specification {

    def "should have given cartesianPoints after creation"() {
        given:
        Point cartPoint = new Point(0.0, 1.0)

        when:
        TileCoordinates coordinates = TileCoordinates.fromPoint(cartPoint)

        then:
        coordinates.getCartesianPoint() == cartPoint
    }

    def "should have proper isometric point"() {
        given:
        Point cartPoint = new Point(0.0, 1.0)

        when:
        TileCoordinates coordinates = TileCoordinates.fromPoint(cartPoint)

        then:
        coordinates.getIsoPoint().getX() == -32.0
        coordinates.getIsoPoint().getY() == 16.0
    }

    def "should have correct polygon"() {
        given:
        Point cartPoint = new Point(0.0, 1.0)

        when:
        TileCoordinates coordinates = TileCoordinates.fromPoint(cartPoint)

        then:
        coordinates.getPolygon().xpoints == [-32, 0, 32, 0]
        coordinates.getPolygon().ypoints == [32, 16, 32, 48]
    }

    def "should polygon contain specified coordinates"() {
        given:
        Point cartPoint = new Point(0.0, 1.0)

        when:
        TileCoordinates coordinates = TileCoordinates.fromPoint(cartPoint)

        then:
        coordinates.getPolygon().contains(0, 17) == true
        coordinates.getPolygon().contains(-17, 32) == true
    }

}
