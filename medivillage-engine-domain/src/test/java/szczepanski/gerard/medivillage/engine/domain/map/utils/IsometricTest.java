package szczepanski.gerard.medivillage.engine.domain.map.utils;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class IsometricTest {

    /**
     * Test for checking iso logic
     */
    @Test
    public void isometricTest() {
        double cartX = Isometric.isometricX(2, 2);
        double cartY = Isometric.isometricY(2, 2);

        System.out.println(cartX);
        System.out.println(cartY);
    }
}