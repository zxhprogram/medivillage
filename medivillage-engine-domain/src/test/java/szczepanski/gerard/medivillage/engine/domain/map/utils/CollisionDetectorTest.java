package szczepanski.gerard.medivillage.engine.domain.map.utils;

import org.testng.Assert;
import org.testng.annotations.Test;

import szczepanski.gerard.medivillage.engine.domain.map.Point;
import szczepanski.gerard.medivillage.engine.domain.sprite.Bounds;

import static org.testng.Assert.*;

public class CollisionDetectorTest {
	
	@Test
	public void detectCollisionIfPointIsInBounds() {
		// Arrange
		Point point = new Point(20.0, 20.0);
		
		Bounds bounds = Bounds.builder()
				.x(0.0)
				.y(0.0)
				.width(30.0)
				.height(30.0)
				.build();
		
		// Act
		boolean pointInBounds = CollisionDetector.isPointInBounds(point, bounds);
		
		// Assert
		assertTrue(pointInBounds);
	}
	
	@Test
	public void detectCollisionIfPointIsNotInBounds() {
		// Arrange
		Point point = new Point(20.0, 20.0);
		
		Bounds bounds = Bounds.builder()
				.x(21.0)
				.y(21.0)
				.width(30.0)
				.height(30.0)
				.build();
		
		// Act
		boolean pointInBounds = CollisionDetector.isPointInBounds(point, bounds);
		
		// Assert
		assertFalse(pointInBounds);
	}
}
