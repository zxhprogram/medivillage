package szczepanski.gerard.medivillage.engine.domain.task

import szczepanski.gerard.medivillage.engine.domain.character.GameCharacter
import szczepanski.gerard.medivillage.engine.domain.map.GameMap
import szczepanski.gerard.medivillage.engine.domain.map.terrain.TerrainTile

import static szczepanski.gerard.medivillage.engine.domain.map.terrain.TerrainMove.*

class MoveTaskSpec extends spock.lang.Specification {

    GameMap map = Stub()
    GameCharacter executor = Mock()

    def "should remove task when there is no next move"() {
        given:
        TerrainTile target = TerrainTile.builder().build()
        MoveTask task = new MoveTask(map, executor, target)

        map.nextMoveForTile(_) >> NO_MOVE

        when:
        task.update()

        then:
        1* executor.removeTask()
    }


}
