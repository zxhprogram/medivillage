package szczepanski.gerard.medivillage.engine.input;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.medivillage.common.logger.Console;
import szczepanski.gerard.medivillage.engine.domain.map.Point;
import szczepanski.gerard.medivillage.engine.domain.map.utils.CollisionDetector;
import szczepanski.gerard.medivillage.engine.domain.sprite.Bounds;
import szczepanski.gerard.medivillage.engine.domain.sprite.Sprite;
import szczepanski.gerard.medivillage.engine.domain.sprite.SpriteRegistry;
import szczepanski.gerard.medivillage.engine.engine.EngineContext;

@RequiredArgsConstructor
public class MouseLeftClickDispatcher {
	
	private final SpriteRegistry spriteRegistry;
	
	private final Sprite notSelectedSprite = null;
	
	public void dispatch(Point mapPointClicked) {
		for (Sprite s : spriteRegistry.findAll()) {
			Bounds bounds = s.bounds();
			boolean isPointInBounds = CollisionDetector.isPointInBounds(mapPointClicked, bounds);
			
			if (isPointInBounds) {
				Console.debug("Selected Sprite with id: " + s.getId());
				
				if (EngineContext.get().isSpriteSelected()) {
					if (spriteWasAlreadySelected(s)) {
						break;
					}
				}
				
				EngineContext.get().setSelectedSprite(s);
				return;
			}
		}
		
		EngineContext.get().setSelectedSprite(notSelectedSprite);
	}
	
	private boolean spriteWasAlreadySelected(Sprite s) {
		return EngineContext.get().getSelectedSprite().getId().equals(s.getId());
	}
	
}
