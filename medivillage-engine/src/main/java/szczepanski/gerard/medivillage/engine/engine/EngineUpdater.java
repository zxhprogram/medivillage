package szczepanski.gerard.medivillage.engine.engine;

@FunctionalInterface
public interface EngineUpdater {
	
	void update();
	
}
