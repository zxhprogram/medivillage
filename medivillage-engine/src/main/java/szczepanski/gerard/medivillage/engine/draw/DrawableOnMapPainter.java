package szczepanski.gerard.medivillage.engine.draw;

import java.util.List;

import javafx.scene.canvas.GraphicsContext;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.medivillage.engine.domain.map.DrawableOnMap;
import szczepanski.gerard.medivillage.engine.domain.sprite.Sprite;
import szczepanski.gerard.medivillage.engine.engine.EngineContext;

@RequiredArgsConstructor
public class DrawableOnMapPainter {
	
	public void draw(List<DrawableOnMap> drawables, GraphicsContext context) {
		drawables.stream()
				.sorted((d1, d2) -> { return Double.compare(d1.mapPoint().getY(), d2.mapPoint().getY());})
				.forEach(d -> d.draw(context));
	}
	
	public void drawSelected(GraphicsContext context) {
		if (EngineContext.get().isSpriteSelected()) {
			Sprite selectedSprite = EngineContext.get().getSelectedSprite();
			selectedSprite.drawSelected(context);
		}
	}
	
}
