package szczepanski.gerard.medivillage.engine.character.villager;

import java.util.Collection;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.medivillage.engine.domain.character.villager.Villager;
import szczepanski.gerard.medivillage.engine.domain.character.villager.VillagerRegistry;
import szczepanski.gerard.medivillage.engine.domain.map.terrain.TerrainTile;
import szczepanski.gerard.medivillage.engine.domain.task.MoveTask;
import szczepanski.gerard.medivillage.engine.domain.task.MoveTaskFactory;
import szczepanski.gerard.medivillage.engine.engine.EngineUpdater;

@RequiredArgsConstructor
public class Villagers implements EngineUpdater {
	
	private final VillagerRegistry villagerRegistry;
	private final MoveTaskFactory moveTaskFactory;

	@Override
	public void update() {
		Collection<Villager> villagers = villagerRegistry.findAll();
		villagers.forEach(Villager::updateTask);
	}
	
	public void moveVillager(String id, TerrainTile destinationTile) {
		Villager villager = villagerRegistry.find(id);
		MoveTask moveTask = moveTaskFactory.create(villager, destinationTile);
		villager.applyMoveTask(moveTask);
	}
	
}
