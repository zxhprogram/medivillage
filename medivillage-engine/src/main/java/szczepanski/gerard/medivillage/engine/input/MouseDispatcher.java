package szczepanski.gerard.medivillage.engine.input;

import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.medivillage.engine.domain.map.Point;
import szczepanski.gerard.medivillage.engine.draw.DrawConstants;

@RequiredArgsConstructor
public class MouseDispatcher {
	
	private final MouseLeftClickDispatcher leftClickDispatcher;
	private final MouseRightClickDispatcher rightClickDispatcher; 
	
	public void dispatchMouseEvent(MouseEvent event) {
		double mapX = event.getX() - DrawConstants.MAP_X_OFFSET;
		double mapY = event.getY() - DrawConstants.MAP_Y_OFFSET;
		
		Point mapPointClicked = new Point(mapX, mapY);
		if (event.getButton() == MouseButton.SECONDARY) {
			rightClickDispatcher.dispatch(mapPointClicked);
		} else {
			leftClickDispatcher.dispatch(mapPointClicked);
		}
	}
	
}
