package szczepanski.gerard.medivillage.engine.input;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.medivillage.common.logger.Console;
import szczepanski.gerard.medivillage.engine.domain.character.villager.Villager;
import szczepanski.gerard.medivillage.engine.domain.map.GameMap;
import szczepanski.gerard.medivillage.engine.domain.map.Point;
import szczepanski.gerard.medivillage.engine.domain.map.terrain.TerrainTile;
import szczepanski.gerard.medivillage.engine.domain.task.MoveTask;
import szczepanski.gerard.medivillage.engine.domain.task.MoveTaskFactory;
import szczepanski.gerard.medivillage.engine.engine.EngineContext;

@RequiredArgsConstructor
public class MouseRightClickDispatcher {

    private final GameMap map;
    private final MoveTaskFactory moveTaskFactory;

    // For now only tiles clicked and ugly cast to villager
    public void dispatch(Point mapPointClicked) {
        if (EngineContext.get().isSpriteSelected()) {
            TerrainTile tile = map.tileOfIso(mapPointClicked);
            Villager villager = (Villager) EngineContext.get().getSelectedSprite();

            if (tile != null) {
                Console.debug("Create move task for Villager %s and Tile %s", villager, tile);
                MoveTask moveTask = moveTaskFactory.create(villager, tile);
                villager.applyMoveTask(moveTask);
            }
        }
    }

}
