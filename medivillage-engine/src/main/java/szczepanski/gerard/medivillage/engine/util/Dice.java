package szczepanski.gerard.medivillage.engine.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Dice {
	
	public static int roll1k6() {
		return roll(1, 6);
	}
	
	public static int roll2k6() {
		return roll(2, 6);
	}
	
	public static int roll3k6() {
		return roll(3, 6);
	}
	
	public static int roll(int times, int possibilities) {
		int sum = 0;
		
		for (int i = 0; i < times; i++) {
			sum += (int) Math.abs(Math.random() * (possibilities + 1));
		}
		
		return sum;
	}
	
}
