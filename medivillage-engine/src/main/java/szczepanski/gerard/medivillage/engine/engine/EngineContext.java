package szczepanski.gerard.medivillage.engine.engine;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import szczepanski.gerard.medivillage.engine.domain.sprite.Sprite;

@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class EngineContext {

    private static EngineContext INSTANCE;

    private Sprite selectedSprite;

    public static EngineContext get() {
        return INSTANCE;
    }

    static void inializeOnEngineStart() {
        INSTANCE = new EngineContext();
    }

    public final boolean isSpriteSelected() {
        return selectedSprite != null;
    }

}
