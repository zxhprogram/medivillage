package szczepanski.gerard.medivillage.engine.draw;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class DrawConstants {
	
	public static final double MAP_X_OFFSET = 600;
	public static final double MAP_Y_OFFSET = 200;
	
}
