package szczepanski.gerard.medivillage.engine.engine;

import javafx.event.EventHandler;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.MouseEvent;
import szczepanski.gerard.medivillage.engine.character.villager.Villagers;
import szczepanski.gerard.medivillage.engine.domain.character.villager.VillagerRegistry;
import szczepanski.gerard.medivillage.engine.domain.map.GameMapFactory;
import szczepanski.gerard.medivillage.engine.domain.map.GameMap;
import szczepanski.gerard.medivillage.engine.domain.sprite.SpriteRegistry;
import szczepanski.gerard.medivillage.engine.domain.task.MoveTaskFactory;
import szczepanski.gerard.medivillage.engine.draw.DrawableOnMapPainter;
import szczepanski.gerard.medivillage.engine.input.MouseDispatcher;
import szczepanski.gerard.medivillage.engine.input.MouseLeftClickDispatcher;
import szczepanski.gerard.medivillage.engine.input.MouseRightClickDispatcher;
import szczepanski.gerard.medivillage.engine.scenario.GameScenario;

public class EngineFactory {

	public Engine newGameEngine(Canvas canvas, GameScenario scenario) {
		GameMapFactory gameMapFactory = new GameMapFactory();
		GameMap gameMap = gameMapFactory.loadMap("map01");
		SpriteRegistry spriteRegistry = new SpriteRegistry();

		MoveTaskFactory moveTaskFactory = new MoveTaskFactory(gameMap);

		VillagerRegistry villagerRegistry = new VillagerRegistry();
		Villagers villagerUpdater = new Villagers(villagerRegistry, moveTaskFactory);

		DrawableOnMapPainter drawableOnMapPainter = new DrawableOnMapPainter();
		configureMouseHandler(canvas, spriteRegistry, gameMap, moveTaskFactory);

		GameLogic gameLogic = GameLogic.builder()
				.canvas(canvas)
				.gameMap(gameMap)
				.spriteRegistry(spriteRegistry)
				.villagerService(villagerUpdater)
				.villagerRegistry(villagerRegistry)
				.drawableOnMapPainter(drawableOnMapPainter)
				.build();

		EngineContext.inializeOnEngineStart();
		return new Engine(gameLogic);
	}

	private void configureMouseHandler(Canvas canvas, SpriteRegistry spriteRegistry, GameMap map, MoveTaskFactory moveTaskFactory) {
		MouseLeftClickDispatcher mouseLeftClickDispatcher = new MouseLeftClickDispatcher(spriteRegistry);
		MouseRightClickDispatcher mouseRightClickDispatcher = new MouseRightClickDispatcher(map, moveTaskFactory);
		MouseDispatcher mouseDispatcher = new MouseDispatcher(mouseLeftClickDispatcher, mouseRightClickDispatcher);
		
		canvas.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				mouseDispatcher.dispatchMouseEvent(e);
			}
		});
	}

}
