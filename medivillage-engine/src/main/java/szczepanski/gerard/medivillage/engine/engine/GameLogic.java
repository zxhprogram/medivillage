package szczepanski.gerard.medivillage.engine.engine;

import java.util.Collection;
import java.util.List;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import lombok.AllArgsConstructor;
import lombok.Builder;
import szczepanski.gerard.medivillage.common.collections.CollectionFactory;
import szczepanski.gerard.medivillage.engine.character.villager.Villagers;
import szczepanski.gerard.medivillage.engine.domain.character.villager.Villager;
import szczepanski.gerard.medivillage.engine.domain.character.villager.VillagerRegistry;
import szczepanski.gerard.medivillage.engine.domain.map.DrawableOnMap;
import szczepanski.gerard.medivillage.engine.domain.map.Point;
import szczepanski.gerard.medivillage.engine.domain.map.GameMap;
import szczepanski.gerard.medivillage.engine.domain.sprite.Sprite;
import szczepanski.gerard.medivillage.engine.domain.sprite.SpriteRegistry;
import szczepanski.gerard.medivillage.engine.draw.DrawConstants;
import szczepanski.gerard.medivillage.engine.draw.DrawableOnMapPainter;

@Builder
@AllArgsConstructor
public class GameLogic {
	
	private final Canvas canvas;
	private final GameMap gameMap;
	private final DrawableOnMapPainter drawableOnMapPainter;
	private final Villagers villagerService;
	private final VillagerRegistry villagerRegistry;
	private final SpriteRegistry spriteRegistry;
	
	public final void initialize() {
		testInitVillagers();
	}
	
	private void testInitVillagers() {
		Villager villager = new Villager(gameMap.tileOfIso(new Point(0.0, 17.0)));
		spriteRegistry.add(villager);
		villagerRegistry.add(villager);

//		Villager villager2 = new Villager(gameMap.tileOfIso(new Point(10, 10)));
//		spriteRegistry.add(villager2);
//		villagerRegistry.add(villager2);
//		villagerService.moveVillager(villager.getId(), gameMap.tileOfIso(new Point(14, 14)));
	}

	public final void update() {
		villagerService.update();
	}

	public final void draw() {
		clearScreen();
		setUpDrawingHead();
		drawObjects();
		restoreDrawingHead();
	}
	
	private void setUpDrawingHead() {
		GraphicsContext context = canvas.getGraphicsContext2D();
		context.translate(DrawConstants.MAP_X_OFFSET, DrawConstants.MAP_Y_OFFSET);
	}
	
	private void restoreDrawingHead() {
		GraphicsContext context = canvas.getGraphicsContext2D();
		context.translate(-DrawConstants.MAP_X_OFFSET, -DrawConstants.MAP_Y_OFFSET);
	}
	
	private void clearScreen() {
		GraphicsContext context = canvas.getGraphicsContext2D();
		context.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
	}
	
	private void drawObjects() {
		GraphicsContext context = canvas.getGraphicsContext2D();
		gameMap.draw(context);
		
		List<DrawableOnMap> drawables = CollectionFactory.list();
		Collection<Sprite> sprites = spriteRegistry.findAll();
		drawables.addAll(sprites);
		drawableOnMapPainter.draw(drawables, context);
		drawableOnMapPainter.drawSelected(context);
	}
	
	public final void tickForEverySecond() {
	}

}
