package szczepanski.gerard.medivillage.common.exception;

public class MedivillageRuntimeException extends RuntimeException {

	private static final long serialVersionUID = -293455483678518063L;

	public MedivillageRuntimeException() {
		super();
	}

	public MedivillageRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public MedivillageRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	public MedivillageRuntimeException(String message) {
		super(message);
	}

	public MedivillageRuntimeException(Throwable cause) {
		super(cause);
	}
	
	
	
}
