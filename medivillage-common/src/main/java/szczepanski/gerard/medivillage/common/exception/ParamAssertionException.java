package szczepanski.gerard.medivillage.common.exception;

public class ParamAssertionException extends MedivillageRuntimeException {
	private static final long serialVersionUID = 6489242616119792841L;

	public ParamAssertionException() {
		super();
	}

	public ParamAssertionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ParamAssertionException(String message, Throwable cause) {
		super(message, cause);
	}

	public ParamAssertionException(String message) {
		super(message);
	}

	public ParamAssertionException(Throwable cause) {
		super(cause);
	}
	
	
	
}
