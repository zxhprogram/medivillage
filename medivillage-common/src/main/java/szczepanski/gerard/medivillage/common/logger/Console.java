package szczepanski.gerard.medivillage.common.logger;

import org.apache.log4j.Logger;

/**
 * 
 * @author Gerard Szczepanski
 */
public class Console {
	
	private static final String LOG_NAME = "Medivillage";
	private static final Logger LOG = Logger.getLogger(LOG_NAME);
	
	public static void info(String log) {
		LOG.info(log);
	}
	
	public static void info(String log, Object... args) {
		LOG.info(String.format(log, args));
	}
	
	public static void debug(String log) {
		LOG.debug(log);
	}
	
	public static void debug(String log, Object... args) {
		LOG.debug(String.format(log, args));
	}
	
}
