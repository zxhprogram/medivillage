package szczepanski.gerard.medivillage.view.window;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.paint.Color;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class GameScene {
	
	private final Scene scene;
	private final Canvas canvas;
	
	public static GameScene create() {
		Group root = new Group();
        Canvas canvas = new Canvas(WindowCommons.WINDOW_WIDTH, WindowCommons.WINDOW_HEIGHT);
        root.getChildren().add(canvas);
		Scene scene = new Scene(root, WindowCommons.WINDOW_WIDTH, WindowCommons.WINDOW_HEIGHT);
		scene.setFill(Color.BLACK);
		
		return new GameScene(scene, canvas);
	}
	
}
